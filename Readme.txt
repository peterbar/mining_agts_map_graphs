R code in <mining_agts_map_graphs.R> file will generate the following graphics: 

(1) Maps of active Agreements Between Mining Companies and Aboriginal Peoples or Governments in Canada, as of December 2018. Data source: Natural Resources Canada. "Indigenous Mining Agreements, Lands and Minerals Sector." http://atlas.gc.ca/imaema/en/

(2) Graphics showing numbers on the Agreements Between Mining Companies and Aboriginal Peoples or Governments in Canada, as of 2013. Data source: Natural Resources Canada. 2013. "Mining Sector Performance Report 1998-2012. Energy and Mines Minister’s Conference." http://www.nrcan.gc.ca/sites/www.nrcan.gc.ca/files/mineralsmetals/files/pdf/MSP-report-eng.pdf.

Created for: Coates, Ken and Carin Holroyd. 2018. "Natural Resources and Aboriginal Autonomy: Economic Development and the Boundaries of Indigenous Control and Engagement" in the "Reclaiming Indigenous Self-Governance: Reflections from the CANZUS Countries," edited by William Nikolakis, Stephen Cornell, and Harry W. Nelson, Chapter 11. Unpublished manuscript, December 12, 2018, typescript in MS Word format. University of Arizona Press.

ADD LICENCE DATA

Citations: 

Natural Resources Canada. 2013. "Mining Sector Performance Report 1998-2012. Energy and Mines Minister’s Conference." Accessed December 12, 2018. http://www.nrcan.gc.ca/sites/www.nrcan.gc.ca/files/mineralsmetals/files/pdf/MSP-report-eng.pdf.

———. 2018. "Indigenous Mining Agreements, Lands and Minerals Sector." Accessed December 18, 2018. http://atlas.gc.ca/imaema/en/

Pebesma, E. 2018. “Simple Features for R: Standardized Support for Spatial Vector Data.” The R Journal. https://journal.r-project.org/archive/2018/RJ-2018-009/.

R Core Team. 2018. “R: A Language and Environment for Statistical Computing.” Vienna: R Foundation for Statistical Computing. https://www.r-project.org/.

Statistics Canada. 2011. “2011 Census Boundary files. Provinces/territories Cartographic Boundary File.” Accessed December 18, 2018. http://www12.statcan.gc.ca/census-recensement/2011/geo/bound-limit/files-fichiers/gpr_000b11a_e.zip.

———. 2011. “2011 Census Boundary files. Water File - Lakes and rivers (polygons)." Accessed December 18, 2018. http://www12.statcan.gc.ca/census-recensement/2011/geo/bound-limit/files-fichiers/ghy_000c11a_e.zip.

Tennekes, Martijn. 2018. “tmap: Thematic Maps in R.” Journal of Statistical Software 84 (6). doi:10.18637/jss.v084.i06.

Teucher, Andy, and Kenton Russell. 2018. “rmapshaper: Client for ‘mapshaper’ for Geospatial Operations.” https://cran.r-project.org/package=rmapshaper.

Wenseleers, Tom, and Christophe Vanderaa. 2018. “export: Streamlined Export of Graphs and Data Tables.” https://github.com/tomwenseleers/export.

Wickham, Hadley. 2017. “tidyverse: Easily Install and Load the ‘Tidyverse.’” https://cran.r-project.org/package=tidyverse.

